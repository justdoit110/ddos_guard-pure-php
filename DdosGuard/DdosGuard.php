<?php
/**
 * Created by PhpStorm.
 * User: justdoit
 * Date: 09.11.18
 * Time: 23:20
 */

namespace App\DdosGuard;

use App\DdosGuard\GuardStorage\GuardStorage;
use App\DdosGuard\GuardStorage\MemcachedStorage;
use App\DdosGuard\GuardStorage\RedisStorage;


class DdosGuard
{
    /**
     * @var GuardStorage "драйвер" хранилища
     */
    protected $guard_storage;

    /**
     * @var array конфиг
     */
    protected $config;

    /**
     * @var string ключ для списка записей посетителей
     */
    protected $access_key;

    /**
     * @var string ключ для списка заблокированных IP
     */
    protected $blocked_key;

    public function __construct()
    {

        $this->config = (require __DIR__ . "/config.php")['guard'];

        switch ($this->config['storage']){
            case 'redis':
                $storage = RedisStorage::class ;
                break;
            case 'memcached':
                $storage = MemcachedStorage::class;
                break;
            default:
                $storage = RedisStorage::class;
        }

        $this->guard_storage = new $storage;
        $this->run();
    }

    /**
     * Entry point
     */
    protected function run(): void
    {
        $this->access_key = $this->config['client_prefix'] . $_SERVER['REMOTE_ADDR'];
        $this->blocked_key = $this->config['block_prefix'] . $_SERVER['REMOTE_ADDR'];

        // проверяем не заблокирован ли IP
        $blocked = $this->isBlocked();

        // вернем ошибку если IP заблокирован
        if( boolval($blocked) ){
            $this->responseBlocked( $blocked['remaining_time'] );
        }

        // если лимиты не превышены добавим новую запись о визите
        if( $this->checkMaxRequests( $this->requestCount() ) ){
            $this->addVisit();
        }

    }

    /**
     * Проверяет находится ли данный IP в списке заблокированных
     *
     * @return bool | array = [remaining_time => (int) Remaining time until unblock]
     */
    protected function isBlocked()
    {
        $ttl = $this->guard_storage->expiration( $this->blocked_key );
        if ($ttl > 0 ) {
            return ['remaining_time' => $ttl];
        }

        return false;
    }

    /**
     * Возвращает кол-во запросов с текущего IP в рамках $this->config['access_time'] времени
     *
     * @return int
     */
    protected function requestCount()
    {
        $requests = count( $this->guard_storage->getValues( $this->access_key ) );

        if( $requests === 0 ){
            $this->addVisit();
            return 1;
        }

        return $requests;

    }

    /**
     * "Блокирует" если текущий запрос превышает лимиты
     *
     * @param $requests
     * @return bool
     */
    protected function checkMaxRequests($requests)
    {
        if( $requests > $this->config['max_requests'] ){

            // добавим IP в список заблокированных
            $this->guard_storage->setKey($this->blocked_key, 1, $this->config['blocked_time']);

            // удалим IP из списка "не заблокированных"
            $this->deleteAccessRecords();

            $this->responseBlocked( $this->config['blocked_time'] );
        }
        return true;
    }

    /**
     * Ответ сервера со статусом "много запросов"
     * @param int $remaining_time время в сек. до разблокировки
     */
    protected function responseBlocked(int $remaining_time )
    {
        header("HTTP/1.1 429 Too Many Requests");
        header("Retry-After: " . $remaining_time);

        exit();
    }

    /**
     * Добавляет запись в список посещений текущего IP
     */
    protected function addVisit()
    {
        return $this->guard_storage->addValue( $this->access_key, 1, $this->config['access_time'] );
    }

    /**
     * Удаляет все записи/визиты с текущего IP
     * @return mixed
     */
    protected function deleteAccessRecords()
    {
        return $this->guard_storage->removeAllValues( $this->access_key );
    }
}