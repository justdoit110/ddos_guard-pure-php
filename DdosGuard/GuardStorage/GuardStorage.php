<?php
/**
 * Created by PhpStorm.
 * User: justdoit
 * Date: 09.11.18
 * Time: 21:57
 */

namespace App\DdosGuard\GuardStorage;


abstract class GuardStorage implements IGuardStorage
{
    protected $config;

    public function __construct()
    {
        $this->config = require __DIR__ . "/../config.php";
    }
}