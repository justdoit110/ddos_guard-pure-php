<?php
/**
 * Created by PhpStorm.
 * User: justdoit
 * Date: 09.11.18
 * Time: 21:56
 */

namespace App\DdosGuard\GuardStorage;

interface IGuardStorage
{

    /**
     * Устанавливает значение ключа
     *
     * @param string $key
     * @param int $ttl
     * @param null $value
     * @return bool
     */
    public function setKey(string $key, $value = null, int $ttl = null);

    /**
     * Возвращает значение ключа
     *
     * @param string $key
     * @return mixed
     */
    public function getKey(string $key);

    /**
     * Добавляет "значение" в запись с ключем $key
     *
     * @param string $key
     * @param null $value
     * @param int|null $ttl
     * @return mixed
     */
    public function addValue(string $key, $value = null, int $ttl = null);

    /**
     * Достает все "значения" в записи с ключем $key
     *
     * @param string $key
     * @return mixed
     */
    public function getValues(string $key):array;

    /**
     * Удаляет все записи по ключу
     * @param string $key
     * @return mixed
     */
    public function removeAllValues(string $key);

    /**
     * Возвращает оставшееся "время жизни" записи
     *
     * @param string $key
     * @return mixed
     */
    public function expiration(string $key):int;

}