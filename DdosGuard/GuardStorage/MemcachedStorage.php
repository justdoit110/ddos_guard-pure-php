<?php
/**
 * Created by PhpStorm.
 * User: justdoit
 * Date: 11.11.18
 * Time: 18:13
 */

namespace App\DdosGuard\GuardStorage;


class MemcachedStorage extends GuardStorage
{

    protected $memc;

    public function __construct()
    {
        parent::__construct();

        $this->memc = new \Memcached();
        $conf = (object)$this->config['guard_storages']['memcached'];
        $this->memc->addServer( $conf->host, $conf->port );
        if(! $this->memc->getStats() ){
            die('Cannot connect to memcached server, host: ' . $conf->host . '; port: ' . $conf->port);
        }
    }

    /**
     * Устанавливает значение ключа
     *
     * @param string $key
     * @param int $ttl
     * @param null $value
     * @return bool
     */
    public function setKey(string $key, $value = null, int $ttl = null)
    {
        $expiration = time() + $ttl;
        $data = [
            'value' => $value,
            'expiration' => $expiration
        ];

        return $this->memc->set( $key, $data, $expiration);
    }

    /**
     * Возвращает значение ключа
     *
     * @param string $key
     * @return mixed
     */
    public function getKey(string $key)
    {
        return $this->memc->get($key);
    }

    /**
     * Добавляет "значение" в запись с ключем $key
     *
     * @param string $key
     * @param null $value
     * @param int|null $ttl
     * @return mixed
     */
    public function addValue(string $key, $value = null, int $ttl = null)
    {
        $data = $this->memc->get($key);

        // удалим просроченные записи, если есть
        $data = boolval($data)? $this->removeExpiredValues($data) : $data;

        $expiration = time() + $ttl;
        $addValue = [
            'item_value'=>1,
            'expiration'=> $expiration
        ];

        $data['values'][] = $addValue;
        $data['expiration'] = $expiration;

        return $this->memc->set( $key, $data, $expiration);
    }

    /**
     * Достает все "значения" в записи с ключем $key
     *
     * @param string $key
     * @return mixed
     */
    public function getValues(string $key): array
    {
        if($data = $this->memc->get($key)) {
            $data = $this->removeExpiredValues($data);
            return $data['values'];
        }
        return [];
    }

    /**
     * Удаляет все записи по ключу
     * @param string $key
     * @return mixed
     */
    public function removeAllValues(string $key)
    {
        return $this->memc->delete( $key );
    }

    /**
     * Возвращает оставшееся "время жизни" записи
     *
     * @param string $key
     * @return mixed
     */
    public function expiration(string $key): int
    {
        if($value = $this->memc->get($key)){
            return (int)$value['expiration'] - time();
        }
        return 0;
    }

    protected function removeExpiredValues( array $items ):array
    {
        foreach ($items['values'] as $k=> &$value){
            if($value['expiration'] < time()){
                unset($items['values'][$k]);
            }
        }
        return $items;
    }
}