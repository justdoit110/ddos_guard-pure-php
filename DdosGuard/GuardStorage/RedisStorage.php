<?php
/**
 * Created by PhpStorm.
 * User: justdoit
 * Date: 09.11.18
 * Time: 23:31
 */

namespace App\DdosGuard\GuardStorage;


class RedisStorage extends GuardStorage
{
    protected $redis;

    public function __construct()
    {
        parent::__construct();

        $this->redis = new \Redis();
        $conf = (object)$this->config['guard_storages']['redis'];
        try{
            $this->redis->connect( $conf->host, $conf->port );
        } catch (\Exception $e){
            echo $e->getMessage(), "<br>";
            echo "Cannot connect to redis server, host: {$conf->host}; port: {$conf->port}";
        }
    }

    /**
     * Устанавливает значение ключа
     *
     * @param string $key
     * @param int $ttl
     * @param null $value
     * @return bool
     */
    public function setKey(string $key, $value = null, int $ttl = null)
    {
        return $this->redis->setex( $key, $ttl, $value);
    }

    /**
     * Возвращает значение ключа
     *
     * @param string $key
     * @return mixed
     */
    public function getKey(string $key)
    {
        return $this->redis->get( $key );
    }

    /**
     * Добавляет "значение" в запись с ключем $key
     *
     * @param string $key
     * @param null $value
     * @param int|null $ttl
     * @return mixed
     */
    public function addValue(string $key, $value = null, int $ttl = null)
    {
        // в случае с redis добавляем новый ключ с уникальным ip в конце
        return $this->redis->setex( uniqid($key), $ttl, $value);
    }

    /**
     * Достает все ключи по шаблону/маске
     *
     * @param string $key
     * @return mixed
     */
    public function getValues(string $key): array
    {
        // в случае с redis берем все "живые" ключи по "маске"
        return $this->redis->keys( $key . "*" );
    }

    /**
     * Возвращает оставшееся "время жизни" записи(ключа)
     *
     * @param string $key
     * @return mixed
     */
    public function expiration(string $key):int
    {
        return $this->redis->ttl( $key );
    }

    /**
     * Удаляет все записи по ключу
     * @param string $key
     * @return mixed
     */
    public function removeAllValues(string $key)
    {
        return $this->redis->delete( $this->redis->keys( $key . "*" ) );
    }
}