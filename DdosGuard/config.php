<?php
/**
 * Created by PhpStorm.
 * User: justdoit
 * Date: 09.11.18
 * Time: 22:41
 */

return [
    'guard' => [
        'max_requests'  => 3,  // per/access_time
        'access_time'   => 20, // (сек.) лимит для <= max_requests
        'blocked_time'  => 10, // (сек.) период блокировки
        'block_prefix'  => 'blocked_', // key prefix for blocked clients
        'client_prefix' => 'client_', // key prefix for visitors
        'storage'       => 'redis', // "драйвер" хранилища из списка guard_storages
    ],

    'guard_storages' => [

        'redis' => [
            'host' => 'ddos_guard_redis',// docker-compose => redis => container_name
            'port'  => '6379',
//            'host' => '127.0.0.1',
//            'port'  => '63790',
        ],

        'memcached' => [
            'host' => 'ddos_guard_memcached', // docker-compose => memcached => container_name
            'port'  => '11211',
//            'host' => '127.0.0.1',
//            'port'  => '11212',
        ]
    ]
];