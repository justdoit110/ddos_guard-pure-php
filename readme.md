[Текст задания](https://goo.gl/7KvTsS)

Демонстрация на docker-контейнерах: php с предустановкой необходимых модулей(phpredis & php-memcached) и 
контейнеры с redis и memcached между которыми можно переключаться (см. ниже).

В папке с "проектом"
```bash
$ docker-compose up 
```
Запуск занимает ~3min.
Контейнер с php запускает `PHP Built-in web server` на 88 порту 
(команда здесь `docker-compose.yml => php => entrypoint`), соответственно 
порт должен быть свободен на локльной машине. 
 
После старта смотреть можно по адресу [http://localhost:88/](http://localhost:88/).
Все настройки описаны и задаются в `./DdosGuard/config.php`

Остановка, удаление контейнеров `Ctrl+C` и: 
```bash
$ docker-compose down
```